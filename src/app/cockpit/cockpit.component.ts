import {Component, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';

@Component({
  selector: 'app-cockpit',
  templateUrl: './cockpit.component.html',
  styleUrls: ['./cockpit.component.css']
})
export class CockpitComponent implements OnInit {

  @Output() serverCreated = new EventEmitter<{serverName: string, serverContent: string}>();
  @Output() blueprintCreated = new EventEmitter<{serverName: string, serverContent: string}>();
  newServerName: string;
  newServerContent: string;
  @ViewChild('serverContentInput') contentInput;

  constructor() { }

  ngOnInit() {
  }

  onAddServer(nameInput, contentInput) {
    this.serverCreated.emit({serverName: nameInput.value, serverContent: this.contentInput.nativeElement.value});
  }

  onAddBlueprint(nameInput, contentInput) {
    console.log(this.contentInput);
    this.blueprintCreated.emit({serverName: nameInput.value, serverContent: this.contentInput.nativeElement.value});
  }

}
