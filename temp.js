function Test(e) {
  var country = e.innerText;
  e.setAttribute('disabled', 'true');
  var span = document.createElement('span');
  e.appendChild(span);
  var time = 0;
  var myTimer = setInterval(function () {
    span.innerText = time;
    time += 0.1;
  }, 100);
  e.classList.remove('btn-primary');
  e.classList.add('btn-success');
  var url = e.getAttribute('content_url');
  var country = e.getAttribute('country');
  var region = e.getAttribute('region');
  var timestamp = now.getTime();
  google.script.run.withSuccessHandler(function (x) {
    if (x) {
      e.classList.add('btn-primary');
      e.classList.remove('btn-success');
      e.removeAttribute('disabled');
      console.log(`Country ${country} took ${time} seconds`);
      span.remove();
    }
  }).remoteStart(url, country, region, timestamp);
}
